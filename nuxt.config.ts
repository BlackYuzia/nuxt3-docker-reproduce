import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    ssr: false,
    css: [
        'vuetify/lib/styles/main.sass',
        // '@mdi/font/css/materialdesignicons.css',
    ],
    modules: [
        // ...
        [
            '@pinia/nuxt',
            {
                autoImports: [
                    // automatically imports `usePinia()`
                    'defineStore',
                    // automatically imports `usePinia()` as `usePiniaStore()`
                    // ['defineStore', 'definePiniaStore'],
                ],
            },
        ],
    ],
    build: {
        transpile: ['vuetify'],
    },
    vite: {
        define: {
            'process.env.DEBUG': false,
        },
    },
    runtimeConfig: {
        public: {
            baseAPI: process.env.BASE_API || "http://localhost:1500"
        }
    },
    app: {
        head: {
            meta: [
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            ],
            // noscript: [
            //     { children: 'Javascript is required' }
            // ]
        }
    }
})
