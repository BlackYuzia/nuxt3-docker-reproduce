# Build
FROM node:lts-alpine as builder

# ENV NODE_ENV production
WORKDIR /home/node

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

# Run
FROM node:lts-alpine as prod

ENV NODE_ENV production
USER node
WORKDIR /home/node

COPY --from=builder --chown=node:node /home/node/package*.json ./
COPY --from=builder --chown=node:node /home/node/.output/ ./.output/

EXPOSE 3000

CMD ["node", ".output/server/index.mjs"]