export const useGayStore = defineStore('gay', {
    state() {
        return {
            gay: false
        }
    },
    getters: {
        getStatus() {
            return this.gay ? "You are a gay (lox)" : "You are not a gay (lucky)";
        }
    },
    actions: {
        magic() {
            console.log("I made you a gay, sorry :3")
            this.gay = true;
        }
    },
    persistedState: {}
})